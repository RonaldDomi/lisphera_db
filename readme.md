This is a starter template for working with Flask projects that can or cannot contain an admin dashboard. 

Installation procedure: 
 -Create virtual environment
    -> Windows: py -m virtualenv venv
    -> Linux: python3 -m virtualenv env
 -Activate venv with 
    -> Windows: .\venv\Scripts\activate
    -> Linux: source venv/bin/activate
 -Install requirements with pip
    -> pip(3) install -r requirements.txt 
