from project import create_app, db
from project.models import User, Record, Achievement, Group, Schedule

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {
        'db': db, 
        'User': User, 
        'Record': Record, 
        'Achievement': Achievement,
        'Group': Group,
        'Schedule': Schedule
    }