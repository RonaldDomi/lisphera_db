from project import db

achievement_table = db.Table(
    'achievement_table',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('achievement_id', db.Integer, db.ForeignKey('achievements.id'))
)

schedule_table_1 = db.Table(
    'schedule_table_1',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('schedule_id', db.Integer, db.ForeignKey('schedules.id'))
)

schedule_table_2 = db.Table(
    'schedule_table_2',
    db.Column('group_id', db.Integer, db.ForeignKey('groups.id')),
    db.Column('schedule_id', db.Integer, db.ForeignKey('schedules.id'))
)


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    role = db.Column(db.String(64), default='athlete')

    records = db.relationship('Record', backref='user', lazy='dynamic')

    achievements = db.relationship('Achievement',
                                secondary=achievement_table,
                                backref=db.backref('users', lazy='dynamic'),
                                lazy='dynamic')

    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'))

    schedules = db.relationship('Schedule',
                            secondary=schedule_table_1,
                            backref=db.backref('users', lazy='dynamic'),
                            lazy='dynamic')


    def __repr__(self):
        return 'User: {}'.format(self.username)

class Record(db.Model):
    """
    One to many relationship.
    One ==>> User
    Many ==>> Record  
    
    record.user
    user.records

    """
    __tablename__ = 'records'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return 'Record: {}'.format(self.name)

class Achievement(db.Model):
    """
    Many to many relationship.
    Many ==>> User
    Many ==>> Achievement  
    
    achievement.users
    user.achievements
    """
    __tablename__='achievements'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    def __repr__(self):
        return 'Achievement: {}'.format(self.name)

class Group(db.Model):
    """
    One to many relationship.
    One ==>> Group
    Many ==>> User
    
    group.user
    user.groups

    """
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    users = db.relationship('User', backref='group', lazy='dynamic')

    schedules = db.relationship('Schedule',
                            secondary=schedule_table_2,
                            backref=db.backref('groups', lazy='dynamic'),
                            lazy='dynamic')

    def __repr__(self):
        return 'Group: {}'.format(self.name)

class Schedule(db.Model):
    """
    1.Many to many relationship.
    Many ==>> User
    Many ==>> Schedule  
    
    schedule.users
    user.schedules

    2.Many to many relationship.
    Many ==>> Group
    Many ==>> Schedule  
    
    schedule.groups
    group.schedules

    """
    __tablename__='schedules'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    def __repr__(self):
        return 'Group: {}'.format(self.name)
