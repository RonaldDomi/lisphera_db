from flask import render_template, redirect, url_for, flash, request, abort
from flask_login import current_user, login_user, logout_user, login_required
# from project.main.forms import LoginForm, RegistrationForm, MockDataForm
from project import db
from project.main import bp

@bp.route('/')
def hello():
    return '<h1> Hello! </h1>'