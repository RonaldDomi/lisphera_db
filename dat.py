db.drop_all()
db.create_all()

ben = User(username='ben')
cao = User(username='cao')
dao = User(username='dao')

rec1 = Record(name='first record')
rec1.user = ben
rec2 = Record(name='first record')
rec2.user = cao

make_100 = Achievement(name='100m')
make_200 = Achievement(name='200m')
make_300 = Achievement(name='300m')

ben.achievements.append(make_100)
ben.achievements.append(make_200)
cao.achievements.append(make_300)
dao.achievements.append(make_200)
dao.achievements.append(make_300)

adults = Group(name='adults')
children = Group(name='children')
ben.group = adults
dao.group = children

schedule1 = Schedule(name='schedule1')
schedule2 = Schedule(name='schedule2')
cao.schedules.append(schedule1)
dao.schedules.append(schedule1)
dao.schedules.append(schedule2)
ben.schedules.append(schedule2)

schedule3 = Schedule(name='schedule3')
children.schedules.append(schedule1)
children.schedules.append(schedule2)
children.schedules.append(schedule3)
adults.schedules.append(schedule2)


db.session.add_all([ben,cao,dao])
db.session.commit()

#0000000000000000

rec1.user
ben.records.all()

make_100.users.all()
ben.achievements.all()

ben.group
children.users.all()

schedule1.users.all()
dao.schedules.all()

schedule2.groups.all()
adults.schedules.all()

#0000000000000000
